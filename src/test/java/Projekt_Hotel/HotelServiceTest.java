package Projekt_Hotel;

import org.assertj.core.internal.cglib.core.Local;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class HotelServiceTest {

    HotelService hotelService;
    @Before
    public void setUp(){
        Hotel hotel = new Hotel();
        this.hotelService= new HotelService(hotel);
    }

    @Test
    public void getAllRooms() {
        List<Room> getRooms = hotelService.getAllRooms();
        for(Room room :getRooms){
            assertThat(room.isAvailable()).isTrue();
        }

    }

    @Test
    public void getAvailableRooms() {
        List<Room> availableRoomslist = hotelService.getAvailableRooms();
        for(Room room :availableRoomslist){
            assertThat(room.isAvailable()).isTrue();
        }
    }

    @Test
    public void bookRoomIsOn() {
        List<Guest> list = new ArrayList<>();
        list.add(new Guest("Sandra","Bednarek",LocalDate.of(1993,Month.FEBRUARY,20)));
        assertThat(hotelService.bookRoom(102,list)).isTrue();
    }
    @Test
    public void bookRoomIsOff() {
        List<Guest> list = new ArrayList<>();
        list.add(new Guest("Sandra","Bednarek",LocalDate.of(1993,Month.FEBRUARY,20)));
        assertThat(hotelService.bookRoom(122,list)).isFalse();
    }

    @Test
    public void getBookedRooms() {
        List<Room> getBookedRooms = hotelService.getBookedRooms();
        for(Room room :getBookedRooms){
            assertThat(room.isAvailable()).isTrue();
        }
    }

    @Test
    public void checkOut() {
        assertThat(hotelService.checkOut(101)).isFalse();
    }

    @Test
    public void getRoomNumber() {
        //assertThat(hotelService.getRoomNumber(101)).isSameAs
    }

    @Test
    public void setGuest() {
    }

    @Test
    public void cleanRoom() {
    }

    @Test
    public void dirtyRooms() {
    }

    @Test
    public void getBookedRoomsDate() {
    }
}