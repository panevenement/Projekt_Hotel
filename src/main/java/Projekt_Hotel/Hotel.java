package Projekt_Hotel;

import java.util.ArrayList;
import java.util.List;

public class Hotel {

    private List<Room> roomList = new ArrayList<>();

    public Hotel() {
        roomList.add(new Room(101, 3, true, true));
        roomList.add(new Room(102, 5, true, true));
        roomList.add(new Room(103, 4, true, true));
        roomList.add(new Room(104, 2, false, true));
        roomList.add(new Room(105, 1, true, true));
        roomList.add(new Room(106, 2, true, true));
    }

    public List<Room> getRoomList() {
        return roomList;
    }

}
