package Projekt_Hotel;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Room {

    private int number;
    private int people;
    private boolean bathroom;
    private boolean available;
    private List<Guest> guests;
    private boolean clean;
    private LocalDate moveIn;
    private LocalDate moveOut;

    public Room(int number, int people, boolean bathroom, boolean available) {
        this.number = number;
        this.people = people;
        this.bathroom = bathroom;
        this.available = available;
        this.guests = new ArrayList<>();
        this.clean = true;
    }


    public boolean isAvailable() {
        return available;
    }

    @Override
    public String toString() {
        return "Room{" +
                "number=" + number +
                ", people=" + people +
                ", bathroom=" + bathroom +
                ", available=" + available +
                ", guests=" + guests +
                '}';
    }

    protected void setAvailable(boolean available) {
        this.available = available;
    }

    public int getNumber() {
        return number;
    }

    public void setGuests(List<Guest> guests) {
        this.guests.addAll(guests);
    }

    public boolean isClean() {
        return clean;
    }

    public void setClean(boolean clean) {
        this.clean = clean;
    }
    public void setMoveIn(){
        this.moveIn=LocalDate.now();
    }
    public void setMoveOut(){
        this.moveOut=LocalDate.now();
    }
    public LocalDate getMoveOut(){
        return LocalDate.now();

    }


}
