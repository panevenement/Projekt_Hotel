package Projekt_Hotel;

import java.time.LocalDate;
import java.util.*;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        Hotel hilton = new Hotel();
        HotelService hiltonService = new HotelService(hilton);


        List<Room> allRooms = hiltonService.getAllRooms();
        List<Room> availableRooms = hiltonService.getAvailableRooms();

        System.out.println("=====ALL ROOMS====");
        for (Room room : allRooms) {
            System.out.println(room.toString());
        }
        System.out.println("=====AVAILABLE ROOMS=====");
        for (Room room : availableRooms) {
            System.out.println(room.toString());
        }

        hiltonService.bookRoom(105, Arrays.asList(
                new Guest("Jan", "Kowalski", LocalDate.parse("1991-01-02")),
                new Guest("Ania", "Ania", LocalDate.parse("1991-02-06"))));
        hiltonService.checkOut(105);
        hiltonService.checkOut(105);

        hiltonService.bookRoom(102,Arrays.asList(new Guest("Jan", "Kowalski", LocalDate.parse("1991-01-02"))));
        hiltonService.cleanRoom(102);

//        Scanner scanner = new Scanner(System.in);
//        System.out.println(" \"Co chcesz zrobić? \" +\n" +
//                "        \"  1) Lista Pokoi\" +\n" +
//                "        \"  2) Lista wolnych pokoi\" +\n" +
//                "        \"  3) Zarezerwój Pokój \" +\n" +
//                "        \"  4) Wymelduj się \"\n" +
//                "    ");
//        int selection = scanner.nextInt();
//        switch (selection) {
//            case 1:
//                System.out.println("=====ALL ROOMS====");
//                for (Room room : allRooms) {
//                    System.out.println(room.toString());
//                }
//                break;
//            case 2:
//                System.out.println("=====AVAILABLE ROOMS=====");
//                for (Room room : availableRooms) {
//                    System.out.println(room.toString());
//                }
//                break;
//            case 3:
//                Scanner sc = new Scanner(System.in);
//                List<Guest> guestList = new ArrayList<>();
//                guestList.add(new Guest("Gość", "Gość", LocalDate.parse("1993-06-02")));
//                System.out.println("Jaki numer pokoju chcesz zarezerować 101-106?");
//                int room = sc.nextInt();
//                if (hiltonService.bookRoom(room, guestList)) {
//                    System.out.println("Rezerwacja przebiegła pomyślnie");
//                } else {
//                    System.err.println("Pokój zajęty");
//                }
//                break;
//            case 4:
//                Scanner out = new Scanner(System.in);
//                System.out.println("Z Jakiego pokoju chcesz się wymeldować?");
//                int roomOut = out.nextInt();
//                if (hiltonService.checkOut(roomOut)) {
//                    System.out.println("Wymeldowano!");
//                } else {
//                    System.err.println("Błąd");
//                    break;
//
//                }
//
//
//
//        }
    }
}
