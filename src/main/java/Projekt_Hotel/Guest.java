package Projekt_Hotel;

import java.time.LocalDate;
import java.time.Period;

public class Guest {

    private String name;
    private String lastName;
    private LocalDate birthday;

    public Guest(String name, String lastName, LocalDate birthday) {
        this.name = name;
        this.lastName = lastName;
        this.birthday = birthday;
    }

    public int getAge() {
        return Period.between(birthday, LocalDate.now()).getYears();
    }


}
