package Projekt_Hotel;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class HotelService {

    private Hotel hotel;

    public HotelService(Hotel hotel) {
        this.hotel = hotel;

    }

    public List<Room> getAllRooms() {
        return hotel.getRoomList();
    }

    public List<Room> getAvailableRooms() {
        List<Room> availableRooms = new ArrayList<>();
        for (Room room : hotel.getRoomList()) {
            if (room.isAvailable()) {
                availableRooms.add(room);

            }
        }
        return availableRooms;

    }

    public boolean bookRoom(int roomNumber, List<Guest> guests) {
        if (isAdult(guests)) {
            for (Room room : getAvailableRooms()) {
                if (room.getNumber() == roomNumber && room.isClean()) {
                    room.setAvailable(false);
                    room.setClean(false);
                    room.setMoveIn();
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isAdult(List<Guest> guests) {
        for (Guest guest : guests) {
            if (guest.getAge() >= 18) {
                return true;
            }
        }
        return false;
    }

    public List<Room> getBookedRooms() {
        List<Room> bookedRooms = new ArrayList<>();
        for (Room room : hotel.getRoomList()) {
            if (!room.isAvailable()) {
                bookedRooms.add(room);

            }
        }
        return bookedRooms;
    }

    public boolean checkOut(int roomNumber) {
        for (Room room : getBookedRooms()) {
            if (room.getNumber() == roomNumber) {
                room.setAvailable(true);
                room.setClean(false);
                room.setMoveOut();
                System.out.println("Udało się wymeldować");
                return true;
            }
        }
        System.err.println("Twój pokój nie jest zajęty!:)");
        return false;
    }

    public Room getRoomNumber(int roomNumber) {
        for (Room room : getAllRooms()) {
            if (room.getNumber() == roomNumber) {
                return room;
            }
        }
        return null;
    }

    public void setGuest(int roomNumber, List<Guest> guests) {
        getRoomNumber(roomNumber).setGuests(guests);
    }

    public Room cleanRoom(int roomNumber) {
        for (Room room : getAllRooms()) {
            if (room.getNumber() == roomNumber) {
                room.setClean(true);
                System.out.println("Posprzątano");


            }
        }
        return null;
    }
    public List<Room> dirtyRooms() {
        List<Room> dirtyRooms= new ArrayList<>();
        for (Room room : getBookedRooms()) {
            if (!room.isClean()) {
                dirtyRooms.add(room);

            }
        }
        return dirtyRooms;
    }
    public List<Room> getBookedRoomsDate() {
        List<Room> bookedRoomsDate = new ArrayList<>();
        for (Room room : hotel.getRoomList()) {
            if (!room.isAvailable()) {
                bookedRoomsDate.add(room);
                

            }
        }
        return bookedRoomsDate;
    }

}


